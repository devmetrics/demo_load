var async = require( 'async' ),
    _ = require( 'underscore' ),
    passport = require( 'passport' ),
    passportGoogleStrategy = require( 'passport-google-oauth' ).OAuth2Strategy,
    keystone = require( 'keystone' ),
    User = keystone.list( 'User' ),
    credentials = {
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: process.env.GOOGLE_CALLBACK_URL,
        scope: 'profile email'
    };

exports.authenticateUser = function ( req, res, next ){
    var redirect = req.cookies.target && req.cookies.target == 'app'
            ? '/auth/app'
            : '/auth/confirm',
        googleStrategy = new passportGoogleStrategy( credentials, function ( accessToken, refreshToken, profile, done ){
            done( null, {
                accessToken: accessToken,
                refreshToken: refreshToken,
                profile: profile
            });
        });

    passport.use( googleStrategy );

    if ( _.has( req.query, 'cb' ) ){
        passport.authenticate( 'google', {session: false}, function ( err, data, info ){
            if ( err || ! data ){
                console.log( "[services.google] - Error retrieving Google account data - " + JSON.stringify( err ) );
                return res.redirect( '/signin' );
            }

            var auth = {
                type: 'google',
                name: {
                    first: data.profile.name.givenName,
                    last: data.profile.name.familyName
                },
                email: data.profile.emails.length ? _.first( data.profile.emails ).value : null,
                website: data.profile._json.blog,
                profileId: data.profile.id,
                username: data.profile.username,
                avatar: data.profile._json.picture,
                accessToken: data.accessToken,
                refreshToken: data.refreshToken
            };

            req.session.auth = auth;
            return res.redirect( redirect );
        } )( req, res, next );
    }
    else
        passport.authenticate( 'google', {accessType: 'offline'} )( req, res, next );
};
