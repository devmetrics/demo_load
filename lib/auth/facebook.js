var async = require( 'async' ),
    passport = require( 'passport' ),
    passportFbStrategy = require( 'passport-facebook' ).Strategy,
    keystone = require( 'keystone' ),
    _ = require( 'underscore' ),
    User = keystone.list( 'User' ),
    credentials = {
        clientID: process.env.FACEBOOK_CLIENT_ID,
        clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
        callbackURL: process.env.FACEBOOK_CALLBACK_URL
    };

exports.authenticateUser = function ( req, res, next ){
    var redirect = req.cookies.target && req.cookies.target == 'app'
            ? '/auth/app'
            : '/auth/confirm',
        facebookStrategy = new passportFbStrategy( credentials, function ( accessToken, refreshToken, profile, done ){
            done( null, {
                accessToken: accessToken,
                refreshToken: refreshToken,
                profile: profile
            });
        });

    passport.use( facebookStrategy );

    if ( _.has(req.query, 'cb') ){
        passport.authenticate( 'facebook', {session: false}, function ( error, data, info ){
            if ( error || ! data ){
                console.log( "[services.facebook] - Error retrieving Facebook account data - " + JSON.stringify(error) );
                redirect = '/signin';
            }
            else {
                var name = data.profile && data.profile.displayName ? data.profile.displayName.split( ' ' ) : [];
                req.session.auth = {
                    type: 'facebook',
                    name: {
                        first: name.length ? name[0] : '',
                        last: name.length > 1 ? name[1] : ''
                    },
                    email: data.profile.emails.length ? data.profile.emails[0].value : null,
                    website: data.profile._json.blog,
                    profileId: data.profile.id,
                    username: data.profile.username,
                    avatar: 'https://graph.facebook.com/' + data.profile.id + '/picture?width=600&height=600',
                    accessToken: data.accessToken,
                    refreshToken: data.refreshToken
                };
            }

            return res.redirect( redirect );
        })( req, res, next );
    }
    else
        passport.authenticate( 'facebook', {scope: ['email']} )( req, res, next );
};
