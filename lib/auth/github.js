var async = require( 'async' ),
    request = require( 'request' ),
    passport = require( 'passport' ),
    passportGithubStrategy = require( 'passport-github' ).Strategy,
    keystone = require( 'keystone' ),
    _ = require( 'underscore' ),
    User = keystone.list( 'User' ),
    credentials = {
        clientID: process.env.GITHUB_CLIENT_ID,
        clientSecret: process.env.GITHUB_CLIENT_SECRET,
        callbackURL: process.env.GITHUB_CALLBACK_URL
    };


exports.authenticateUser = function ( req, res, next ){
    var redirect = req.cookies.target && req.cookies.target == 'app'
            ? '/auth/app'
            : '/auth/confirm',
        githubStrategy = new passportGithubStrategy( credentials, function( accessToken, refreshToken, profile, done ){
            done( null, {
                accessToken: accessToken,
                refreshToken: refreshToken,
                profile: profile
            });
        });

    passport.use( githubStrategy );

    if ( _.has(req.query, 'cb') ){
        passport.authenticate( 'github', {session: false}, function ( error, data, info ){
            if ( error || !data ){
                console.log( "[services.github] - Error retrieving GitHub account data - " + JSON.stringify( error ) );
                res.redirect( '/signin' );
            }
            else {
                var name = data.profile && data.profile.displayName ? data.profile.displayName.split( ' ' ) : [],
                    auth = {
                        type: 'github',
                        name: {
                            first: name.length ? name[0] : '',
                            last: name.length > 1 ? name[1] : ''
                        },
                        website: data.profile._json.blog,
                        profileId: data.profile.id,
                        username: data.profile.username,
                        avatar: data.profile._json.avatar_url,
                        accessToken: data.accessToken,
                        refreshToken: data.refreshToken
                    };

                exports.getEmails( auth.accessToken, function ( error, email ){
                    if ( !error && email )
                        auth.email = email;
                    req.session.auth = auth;
                    res.redirect( redirect );
                });
            }
        })( req, res, next );
    }
    else
        passport.authenticate( 'github', {scope: ['user:email']} )( req, res, next );
};


exports.getEmails = function ( accessToken, callback ){
    request( {
        url: 'https://api.github.com/user/emails?access_token=' + accessToken,
        headers: {'User-Agent': 'devmetrics.io'}
    }, function ( error, data ){
        if ( error ){
            console.log(err);
            console.log('[services.github] - Error retrieving GitHub email addresses.');
            console.log('------------------------------------------------------------');
            callback( error );
        }
        else {
            var emails = JSON.parse( data.body ),
                primaryEmail = false;

            if ( emails.length ){
                emails.some( function ( e ){
                    if ( e.primary )
                        primaryEmail = e.email;
                    return e.primary;
                });
                if ( !primaryEmail )
                    primaryEmail = emails.pop();
            }

            callback( error, primaryEmail );
        }
    });
};
