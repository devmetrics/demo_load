var keystone = require( 'keystone' ),
    Types = keystone.Field.Types,
    User = new keystone.List( 'User', {
        track: true,
        autokey: {path: 'key', from: 'name', unique: true}
    }),
    deps = {
        github: { 'services.github.isConfigured': true },
        facebook: { 'services.facebook.isConfigured': true },
        google: { 'services.google.isConfigured': true }
    };

User.add({
    name: {type: Types.Name, required: true, index: true},
    email: {type: Types.Email, initial: true, required: true, index: true},
    password: {type: Types.Password, initial: true, required: true},
    resetPasswordKey: {type: String, hidden: true}
}, 'Profile', {
    photo: {type: Types.CloudinaryImage},
    github: {type: String, width: 'short'},
    website: {type: Types.Url},
    bio: {type: Types.Markdown},
    gravatar: {type: String, noedit: true}
}, 'Permissions', {
    isAdmin: {type: Boolean, label: 'Can access Keystone', index: true}
}, 'Services', {
    services: {
        github: {
            isConfigured: {type: Boolean, label: 'GitHub has been authenticated'},
            profileId: {type: String, label: 'Profile ID', dependsOn: deps.github},
            username: {type: String, label: 'Username', dependsOn: deps.github},
            avatar: {type: String, label: 'Image', dependsOn: deps.github},
            accessToken: {type: String, label: 'Access Token', dependsOn: deps.github},
            refreshToken: {type: String, label: 'Refresh Token', dependsOn: deps.github}
        },
        facebook: {
            isConfigured: {type: Boolean, label: 'Facebook has been authenticated'},
            profileId: {type: String, label: 'Profile ID', dependsOn: deps.facebook},
            username: {type: String, label: 'Username', dependsOn: deps.facebook},
            avatar: {type: String, label: 'Image', dependsOn: deps.facebook},
            accessToken: {type: String, label: 'Access Token', dependsOn: deps.facebook},
            refreshToken: {type: String, label: 'Refresh Token', dependsOn: deps.facebook}
        },
        google: {
            isConfigured: {type: Boolean, label: 'Google has been authenticated'},
            profileId: {type: String, label: 'Profile ID', dependsOn: deps.google},
            username: {type: String, label: 'Username', dependsOn: deps.google},
            avatar: {type: String, label: 'Image', dependsOn: deps.google},
            accessToken: {type: String, label: 'Access Token', dependsOn: deps.google},
            refreshToken: {type: String, label: 'Refresh Token', dependsOn: deps.google}
        }
    }
});


User.schema.virtual( 'canAccessKeystone' ).get( function (){
    return this.isAdmin;
});

User.schema.virtual( 'githubUsername' ).get( function(){
    return (this.services.github && this.services.github.isConfigured) ? this.services.github.username : '';
});


User.schema.methods.resetPassword = function(callback) {
    var user = this;
    user.resetPasswordKey = keystone.utils.randomString( [16,24] );
    user.save( function( error ){
        if ( error )
            callback( error );
        else
            new keystone.Email( 'forgotten-password' ).send({
                user: user,
                link: '/reset-password/' + user.resetPasswordKey,
                subject: 'Reset your devemetrics.io Password',
                to: user.email,
                from: {
                    name: 'devemetrics.io',
                    email: 'contact@devmetrics.io'
                }
            }, callback );
    });
};


User.relationship( {ref: 'Post', path: 'posts', refPath: 'author'} );
User.defaultColumns = 'name, email, isAdmin';
User.register();
