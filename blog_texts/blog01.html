﻿<h1 id="s1">Introduction</h1>
<p>In this post I will provide a 5-minute guide showing how to monitor a <a href="https://nodejs.org/" target="_blank">Node.js</a> application in production environment, how to track exceptions and investigate their reasons, how to measure speed of your application and to detect slow requests and possible bottlenecks.</p>
<p>This post focuses on a <a href="http://mean.io/" target="_blank">MEAN</a> stack application, which is generally used by websites to render some web pages and to respond to Ajax and API calls. I will review:</p>
<ul>
    <li>Why you need monitoring?</li>
    <li>What to monitor?</li>
    <li>How to quickly setup fully open source monitoring solution?</li>
    <li>Code samples that let you add logs and monitoring metrics to your application.</li>
</ul>
<p>In future posts, we'll discuss best practices for monitoring for other types of applications and go deeper into separate aspects of monitoring like metrics, exception handling and others. We welcome your comments or questions in our comments section.</p>
<h1 id="s2">Why Monitoring?</h1>
<p>When developers run their application locally, it&rsquo;s easy for them to track application behavior and discover possible problems using common monitoring techniques:</p>
<ul>
    <li>stdout/stderr</li>
    <li>log files/syslog</li>
    <li>debug mode, breakpoints</li>
</ul>
<p>However, as the application progresses towards production deployment, monitoring needs change:</p>
<ul>
    <li>there are many simultaneous sessions and requests from different users, which can end-up being mixed together in the logs</li>
    <li>user actions are unpredictable and even when you have an exception log, it&rsquo;s often hard to reproduce the user's failure; you need more information to successfully investigate the failure</li>
    <li>usually no debugger available in production</li>
</ul>
<h1>Set up Monitoring in 5 Minutes</h1>
<p>Let&rsquo;s set up the monitoring. We will use the npm library <a href="https://www.npmjs.com/package/devmetrics" target="_blank">devmetrics</a>.</p>
<p><strong>Important!</strong> Before you start, you must decide if you want to use devmetrics.io shared monitoring instance or request a private instance. If any of your data is sensitive or private, then please <a href="../../contact?msg=I%20would%20like%20to%20get%20a%20dedicated%20monitoring%20instance.#message" target="_blank">request a dedicated instance</a> and redirect the output of the library to it.</p>
<p>To set up the solution in devmetrics.io shared environment, add the npm module <a href="https://www.npmjs.com/package/devmetrics" target="_blank">devmetrics</a> to your application. The module will collect log data and metrics, and then store them to a monitoring database. As data and metrics are pushed to the database, you can use the devmetrics.io dashboards to watch your application's performance.</p>
<ol>
    <li>
        <p>Configure the npm module <a href="https://www.npmjs.com/package/devmetrics">devmetrics</a> module by selecting the option that is appropriate for you:</p>
        <table>
            <tbody>
            <tr><th>Add devmetrics to your app</th><th>Use our template app</th></tr>
            <tr>
                <td>Run:
                    <pre>npm install devmetrics</pre>
                    Add code to your config/express.js:
                    <pre><code>require('devmetrics')({'app':app});</code></pre>
                </td>
                <td>Download preconfigured <a href="https://github.com/devmetrics/devmetrics-nodejs-example">Example App</a><br />and use it as template</td>
            </tr>
            </tbody>
        </table>
    </li>
    <li>
        <p>Restart your app and make sure you see new logs in stdout:</p>
        <img src="../../images/blog01/02_stdout.png" alt="" />
        <p>If you do not see logs, then <a href="../../contact?msg=I%20installed%20the%20npm%20module%20%27devmetrics%27,%20but%20I%20do%20not%20see%20logs.#message"> send us details </a> and we will help you find a solution.</p>
    </li>
    <li>
        <p>Open the link you see in stdout and enjoy your dashboard:</p>
        <img src="../../images/blog01/03_linktodashboard.png" alt="" width="300" /></li>
</ol>
<p><img src="../../images/blog01/04_dashboard.png" alt="" width="300" /></p>
<h1>What Information is Collected?</h1>
<p>First, let&rsquo;s discuss what data is collected and why. Here is the scheme of a typical <a href="http://mean.io/" target="_blank">MEAN</a> app:</p>
<p><img src="../../images/blog01/01_meanapp.png" alt="" /></p>
<p>The NODE (API) block provides an application interface (API) for the front-end, and sometimes for external software (mobile clients such as iOS and Android). To serve its client components, the NODE block reads and writes data from/to the database.</p>
<p>To get complete and actionable telemetry, the library collects three types of information:</p>
<ol>
    <li>API requests: access log for your application, the request time, and the code of the http response.</li>
    <li>Anomalies that occur during internal-application execution (such as handled exceptions, unhandled exceptions, unexpected input, developer bugs, and timeouts).</li>
    <li>Database read and write requests.</li>
</ol>
<h2 id="s5">API Requests Monitoring</h2>
<p>The <a href="http://mean.io/">MEAN</a> stack uses the <a href="http://expressjs.com/">Express</a> framework for each API request. This framework provides URL routing and handles requests.</p>
<p>For each API call the devmetrics library collects the following information:</p>
<table>
    <tbody>
    <tr><th>data captured</th><th>why you need the data</th></tr>
    <tr>
        <td>uri</td>
        <td>Shows the action that was triggered and what part of code was executed.</td>
    </tr>
    <tr>
        <td>status_code</td>
        <td>Shows the result of the execution: 200 for OK, 500 for error, and <a href="http://www.w3.org/Protocols/HTTP/HTRESP.html">others</a>.</td>
    </tr>
    <tr>
        <td>response_time</td>
        <td>Shows how fast the request was executed, 200 for OK, 500 for response after 5 seconds of waiting.</td>
    </tr>
    <tr>
        <td>timestamp</td>
        <td>Allows you to filter logs by time.</td>
    </tr>
    <tr>
        <td>host</td>
        <td>If you have more than one application server, then you can set-up separate data collection, which allows you to troubleshoot different behavior/problems on different servers.</td>
    </tr>
    <tr>
        <td>session_id</td>
        <td>Allows you to reconstruct user behavior and events by filtering logs by session_id.</td>
    </tr>
    </tbody>
</table>
<h2>Application Code (Business Logic) Monitoring</h2>
<p>Information about API requests is not enough to understand causes of slow performance and failures. We need more details. We need to know which exact <a href="https://nodejs.org/" target="_blank">Node.js</a> code performs slow or fails. The library provides a mechanism to collect the detailed information about the anomalies of the code execution:</p>
<ol>
    <li>Handled exceptions</li>
    <li>Unhandled exceptions</li>
    <li>Slowly performing paths</li>
    <li>Unexpected parameter values</li>
</ol>
<p>The mechanism is explained further in this blog.</p>
<h2>Database and Cross-App Calls Monitoring</h2>
<p>The <a href="http://mean.io/" target="_blank">MEAN</a> app, as do many others, depends on data storage. In addition, your app may perform cross-app calls and depend on other services.</p>
<p>Often, when a request time is too long (page opens slowly), the problem may not be in your application code; the problem may be in the database or remote services.</p>
<p>Your javascript code sends request to a service and waits for the answer. The library collects information about all the interactions between your app and external components, so that you can determine if your exceptions or performance issues are caused by your code or by external dependencies.</p>
<h1>Review your Dashboards</h1>
<p>Let&rsquo;s look closer at the dashboards and see how the collected information is represented:</p>
<p>API Requests:</p>
<p><img src="../../images/blog01/05_api-calls-chart.png" alt="" /></p>
<p>Database and cross-app calls:</p>
<p><img src="../../images/blog01/06_cross-app.png" alt="" /></p>
<p>Traffic on the web site:</p>
<p><img src="../../images/blog01/07_traffic.png" alt="" /></p>
<h1>Configure Monitoring for Business Logic</h1>
<p>You will get monitoring for API calls and cross-app calls out of the box. Add some additional lines of code to enable monitoring for business logic.</p>
<h2>Redirect Your Logs</h2>
<p>First, you can replace your console.log with devmetrics' full-featured logger. By using the devmetrics logger, you can move from development to production, without the need to comment out all console.log(&hellip;) lines. You can log development information to debug/trace, and you can log production information to info.</p>
<p>To user logger:</p>
<pre><code>require('devmetrics')().logger('info', 'Something you will see in production');
    require('devmetrics')().logger('debug', 'Dev only lines');</code></pre>
<p><img src="../../images/blog01/10_event-log.png" alt="" /></p>
<h2>Configure Centralized Exception Handling</h2>
<p>If you want to add the devmetrics handler for uncaught exceptions, then add the following option when you initialize our library:</p>
<pre><code>require('devmetrics')({'app': app, 'uncaughtException': true});</code></pre>
<p>We recommend that you add logging to all exception handling in your code. This allows you to collect all the errors, and to view charts with exception counts and drill down to exception details:</p>
<pre><code>try {
    // application code
    ...
    } catch (err) {
    // custom error handling
    require('devmetrics')().exception(err);
    }</code></pre>
<h2>Configure Handler for Thread Restart</h2>
<p>A thread may restart either because of operational activity (such as a release or upgrade) or because of an unhandled exception. Every time a thread restarts, your application performance may be affected.</p>
<p>If you have a log for each restart, then you can see how often your app restarts and which part of the restart is accompanied by an unhandled exception.</p>
<p>Open <strong>server.js</strong> and add the restart handler:</p>
<pre><code>mean.serve({ workerid: workerId /* more options placeholder*/ }, function (app) {
    var logger = require('devmetrics')().app_event('app_started');
    });
    )
</code></pre>
<h2>Measure Function Execution Time</h2>
<p>To measure a specific function execution time, you can wrap it with the devmetrics function wrapper. This enables you to see how often your function is called and how many milliseconds it takes to execute.</p>
<pre><code>
    function myFunc() {...}
    myFunc = require('devmetrics')().funcWrap(myFunc, 'NameForDashboard');
</code></pre>
<h1>Summary</h1>
<p>We reviewed three types of information that should be collected in order to get representative monitoring data: incoming calls, anomalies of internal application execution, and outgoing calls like requests to database and cross-application calls.</p>
<p>I showed you how to use a monitoring library in order to get basic monitoring and how to instrument the code in order to make monitoring actionable.</p>
<h1>What Is Next?</h1>
<p>In upcoming posts, I will cover:</p>
<ul>
    <li>Metrics: types of metrics, what to measure, how to measure, and how to chart.</li>
    <li>Set-up a private instance to collect and store logs and metrics, using only open source software components.</li>
    <li>Code profiling in production: adding measure functions execution time or some code lines execution time.</li>
    <li>Usage analysis: adding business metrics to user interactions.</li>
</ul>