var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    jshintReporter = require('jshint-stylish'),
    watch = require('gulp-watch');

var paths = {
    'src':['./models/**/*.js','./routes/**/*.js', 'keystone.js', 'package.json']
};

gulp.task('lint', function(){
    gulp.src(paths.src)
        .pipe(jshint())
        .pipe(jshint.reporter(jshintReporter));
});

gulp.task('watch:lint', function () {
    gulp.src(paths.src)
        .pipe(watch())
        .pipe(jshint())
        .pipe(jshint.reporter(jshintReporter));
});
