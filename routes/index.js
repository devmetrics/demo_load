var keystone = require( 'keystone' ),
    middleware = require( './middleware' ),
    importRoutes = keystone.importer( __dirname );

keystone.pre( 'routes', middleware.initLocals );
keystone.pre( 'render', middleware.flashMessages );

var routes = {
    views: importRoutes( './views' ),
    auth: importRoutes( './auth' )
};

exports = module.exports = function ( app ){
    var host = process.env.DEVMETRICS_HOST || 'lab.devmetrics.io';
    var token = process.env.DEVMETRICS_TOKEN || 'token-123128';
    var devmetrics_options = {'app': app, 'host': host, 'token': token, 'uncaughtException' : true};
    devmetrics = require('devmetrics')(devmetrics_options);

    app.get( '/', routes.views.index );
    app.get( '/blog/:category?', routes.views.blog );
    app.get( '/blog/post/:post', routes.views.post );
    app.all( '/contact', routes.views.contact );
    app.all( '/about', routes.views.about);
    app.get( '/dashboard/:app_id', routes.views.dashboard);

    app.all( '/join', routes.views.session.join );
    app.all( '/signin', routes.views.session.signin );
    app.get( '/signout', routes.views.session.signout );
    app.all( '/forgot-password', routes.views.session['forgot-password'] );
    app.all( '/reset-password/:key', routes.views.session['reset-password'] );
    app.all( '/auth/confirm', routes.auth.confirm );
    app.all( '/auth/app', routes.auth.app );
    app.all( '/auth/:service', routes.auth.service );

    app.get('/dashboard_legacy/:app_id', function (req, res) {
        var app_id = req.params.app_id;

        // do post to create dashboards
        // An object of options to indicate where to post to
        var post_options = {
          host: 'service.devmetrics.io',
          port: '80',
          path: '/api/apps/' + app_id,
          method: 'PUT'
        };
        // Set up the request
        var http = require('http');
        var post_req = http.request(post_options, function(res) {
          res.setEncoding('utf8');
          res.on('data', function (chunk) {
            devmetrics.logger('info', 'Response from dashboards script: ' + chunk);
          });
        });
        // post the data
        post_req.write('');
        post_req.end();

        //@todo: check response and show error when needed
        res.redirect('http://service.devmetrics.io/apps/' + app_id + '/#/dashboard/summary');
        return res;
      }
    );

    var eventCounter = function(req, res) {
      var app_id = req.params.app_id;
      var request = require('request');
      request('http://service.devmetrics.io/api/apps/' + app_id, function (error, response, body) {
        if (!error && response.statusCode == 200) {
          var ans = JSON.parse(body);
          res.json(ans);
        } else {
          res.statusCode = 500;
          devmetrics.logger('error', JSON.stringify(response));
          res.json(error);
        }
      });
    };
    eventCounter = devmetrics.funcWrap(eventCounter, 'eventCounter');

    app.get('/api/:app_id/events/count', eventCounter);

    app.post('/api/:app_id/dashboard', function(req, res) {
      var app_id = req.params.app_id;
      var request = require('request');
      request.put('http://service.devmetrics.io/api/apps/' + app_id, function (error, response, body) {
        if (!error && response.statusCode == 200) {
          var ans = JSON.parse(body);
          res.json(ans);
        } else {
          res.statusCode = 500;
          devmetrics.logger('error', JSON.stringify(response));
          res.json(error);
        }
      });
    });
};
