var _ = require( 'underscore' ),
    keystone = require( 'keystone' );


exports.initLocals = function( req, res, next ){
    var locals = res.locals;
    locals.navLinks = [
        {label: 'Home', key: 'index', href: '/'},
        {label: 'Blog', key: 'blog', href: '/blog'},
//        {label: 'GitHub', key: 'github', href: 'https://github.com/devmetrics/devmetrics-nodejs'},
        {label: 'Contact Us', key: 'contact', href: '/contact'}
//        {label: 'About Us', key: 'about', href: '/about'}
    ];
    locals.user = req.user;
    locals.page = {
        title: 'devemetrics.io',
        path: req.url.split( "?" )[0]
    };
    locals.siteUrl = keystone.get( 'siteUrl' );

    next();
};


exports.flashMessages = function( req, res, next ){
    var flashMessages = {
        info: req.flash( 'info' ),
        success: req.flash( 'success' ),
        warning: req.flash( 'warning' ),
        error: req.flash( 'error' )
    };

    res.locals.messages = _.any( flashMessages, function( msgs ){
        return msgs.length;
    }) && flashMessages;
    next();
};


exports.requireUser = function ( req, res, next ){
    if ( ! req.user ){
        req.flash( 'error', 'Please sign in to access this page.' );
        res.redirect( '/keystone/signin' );
    }
    else
        next();
};
