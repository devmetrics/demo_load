var keystone = require('keystone');

exports = module.exports = function (req, res) {
  var view = new keystone.View(req, res),
    locals = res.locals;

  var app_id = req.params.app_id;

  locals.section = 'dashboard';
  view.render('dashboard', {'app_id': app_id});
};
