/**
 * @author Marenin Alexander
 * @date April 2015
 */

var keystone = require( 'keystone' ),
    async = require( 'async' );

exports = module.exports = function ( req, res ){
    var view = new keystone.View( req, res ),
        locals = res.locals,
        Post = keystone.list( 'Post' ).model,
        PostCategory = keystone.list( 'PostCategory' ).model;

    locals.section = 'blog';
    locals.filters = {
        category: req.params.category
    };
    locals.data = {
        posts: [],
        categories: [],
        search: String( req.query.search || '' ).trim()
    };

    view.on( 'init', function ( next ){
        PostCategory.find().sort( 'name' ).exec( function ( err, results ){
            if ( err || ! results.length )
                return next( err );

            locals.data.categories = results;
            async.each( locals.data.categories, function ( category, next ){
                Post.count().where( 'category' ).in( [category.id] ).exec( function ( err, count ){
                    category.postCount = count;
                    next( err );
                });
            }, function ( err ){
                next( err );
            });
        });
    });


    view.on( 'init', function ( next ){
        if ( req.params.category )
            PostCategory.findOne( {key: locals.filters.category} ).exec( function ( err, result ){
                locals.data.category = result;
                next( err );
            });
        else
            next();
    });


    view.on( 'init', function ( next ){
        var q = keystone.list( 'Post' ).paginate({
                page: req.query.page || 1,
                perPage: 10,
                maxPages: 10
            })
            .where( 'state', 'published' )
            .sort( '-publishedDate' )
            .populate( 'author categories' );

        if ( locals.data.search ){
            var re = new RegExp( stripTags(locals.data.search), 'i' );
            q.or( [{title: re}, {'content.extended': re}] );
        }

        if ( locals.data.category )
            q.where( 'categories' ).in( [locals.data.category] );

        q.exec( function ( err, results ){
            locals.data.posts = results;
            next( err );
        });
    });

    view.render( 'blog' );
};


/**
 * @param {string} string
 * @returns {string}
 */
function stripTags( string ){
    return String( typeof string === 'undefined' ? '' : string ).replace( stripTags.re, '' );
}
stripTags.re = /<[^>]+>/g;
