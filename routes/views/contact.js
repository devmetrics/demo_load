var keystone = require( 'keystone' ),
    Enquiry = keystone.list( 'Enquiry' );

exports = module.exports = function ( req, res ){
    var view = new keystone.View( req, res ),
        locals = res.locals;

    locals.section = 'contact';
    locals.enquiryTypes = Enquiry.fields.enquiryType.ops;
    locals.formData = req.body || {};
    locals.validationErrors = {};
    locals.enquirySubmitted = false;

    view.on( 'post', {action: 'contact'}, function ( next ){
        var newEnquiry = new Enquiry.model(),
            updater = newEnquiry.getUpdateHandler( req );
  
        updater.process( req.body, {
            flashErrors: true,
            fields: 'name, email, phone, enquiryType, message',
            errorMessage: 'There was a problem submitting your enquiry:'
        }, function ( err ){
//VN 2015-05-12 Unhandled Exception
            try{
                if (newEnquiry.name.first == 'Unhandled'){
                    RaiseUnhandledException();
                }
//                console.log("name = " + newEnquiry.name );
             } catch(e) {
                    require('devmetrics')().exception(e);
                    return res.status(500).json({error: 'Internal Server Error' });
            }
// End
            if ( err ){
                locals.validationErrors = err.errors;
            }
            else
                locals.enquirySubmitted = true;
            next();
        });
    });

    try {
      foo();
    } catch(e) {
        require('devmetrics')().exception(e);
    }

    view.render( 'contact' );
};
