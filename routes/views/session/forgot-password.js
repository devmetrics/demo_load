var keystone = require( 'keystone' ),
    User = keystone.list( 'User' );

module.exports = function ( req, res ){
    var view = new keystone.View( req, res ),
        locals = res.locals;


    view.on( 'post', {action: 'forgot-password'}, function ( next ){
        if ( ! req.body.email ){
            req.flash( 'error', "Please enter an email address." );
            return next();
        }

        User.model.findOne().where( 'email', req.body.email ).exec( function ( error, user ){
            if ( error )
                next( error );
            else if ( !user ){
                req.flash( 'error', "Sorry, we don't recognise that email address." );
                return next();
            }
            else
                user.resetPassword( function( error ){
                    if ( error ){
                        console.error( error );
                        req.flash( 'error', 'Error sending reset password email.' );
                        next();
                    }
                    else {
                        req.flash( 'success', 'We have emailed you a link to reset your password' );
                        res.redirect( '/signin' );
                    }
                });
        });
    });

    view.render( 'session/forgot-password' );
};
