var keystone = require( 'keystone' );

exports = module.exports = function ( req, res ){
    var view = new keystone.View( req, res ),
        locals = res.locals;

    view.on( 'init', function( next ){
        keystone.list( 'Post' )
            .model
            .find()
            .where( 'state', 'published' )
            .sort( '-publishedDate' )
            .limit( 3 )
            .populate( 'author categories' )
            .exec( function( error, results ){
                locals.posts = results;
                next( error );
            });
    });

    locals.section = 'index';
    view.render( 'index' );
};
