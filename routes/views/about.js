var keystone = require( 'keystone' ),
    Enquiry = keystone.list( 'Enquiry' );

exports = module.exports = function ( req, res ){
    var view = new keystone.View( req, res ),
        locals = res.locals;

    // tmp working save
//    var userData = {
//        name: {
//          first: 'a',
//          last: 'b'
//        },
//        email: 'a',
//        password: 'a',
//        website: 'a'
//      },
//      User = keystone.list( 'User' ).model,
//      newUser = new User( userData );
//
//      newUser.save( function ( err ){
//        console.log('saved res');
//      });


  // tmp just generates db error
  if (false) {
    var userData = {},
      User = keystone.list('User').model,
      newUser = new User(userData);

    console.log('>>>aaa');
    console.log(newUser.save);
    console.log(newUser);

    newUser.save(function (err) {
      console.log('saved res');
      console.log(err);
    });
  }
    // end of tmp gen error code


    locals.section = 'about';
    locals.enquiryTypes = Enquiry.fields.enquiryType.ops;

    view.render( 'about' );
};
