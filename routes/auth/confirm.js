var keystone = require( 'keystone' ),
    async = require( 'async' ),
    _ = require( 'underscore' ),
    User = keystone.list( 'User' );


module.exports = function ( req, res ){
    var view = new keystone.View( req, res ),
        locals = res.locals;

    locals.section = 'profile';
    locals.form = req.body;
    locals.returnto = req.query.returnto;
    locals.authUser = req.session.auth;
    locals.existingUser = false;

    if ( !locals.authUser )
        return res.redirect( '/signin' );

    if ( req.user )
        locals.existingUser = req.user;

    view.on( 'init', function ( next ){
        return checkExisting( next );
    });

    view.on( 'post', {action: 'confirm.details'}, function ( next ){
        if ( ! locals.form['name.first'] || ! locals.form['name.last'] || ! locals.form.email ){
            req.flash( 'error', 'Please enter a name & email.' );
            return next();
        }
        return checkAuth();
    });

    view.render( 'auth/confirm' );


    function doSignIn(){
        var onSuccess = function ( user ){
                return res.redirect( req.cookies.target || '/' );
            },
            onFail = function ( error ){
                req.flash( 'error', 'Sorry, there was an issue signing you in, please try again.' );
                return res.redirect( '/signin' );
            };
        keystone.session.signin( String( locals.existingUser._id ), req, res, onSuccess, onFail );
    }


    function checkExisting( next ){
        if ( locals.existingUser )
            return checkAuth();

        var query = User.model.findOne();
        query.where( 'services.' + locals.authUser.type + '.profileId', locals.authUser.profileId );
        query.exec( function ( error, user ){
            if ( error ){
                console.log( '[auth.confirm] - Error finding existing user via profile id.', error );
                next( {message: 'Sorry, there was an error processing your information, please try again.'} );
            }
            else if ( user ){
                locals.existingUser = user;
                doSignIn();
            }
            else
                next();
        });
    }


    function checkAuth(){
        async.series({
            checkExistence: function( next ){
                if ( locals.existingUser )
                    return next();

                var query = User.model.findOne();
                query.where( 'email', locals.form.email );
                query.exec( function ( err, user ){
                    if ( err )
                        next( {message: 'Sorry, there was an error processing your information, please try again.'} );
                    else if ( user )
                        next( {message: 'There\'s already an account with that email address, please sign-in instead.'} );
                    else
                        next();
                });
            },

            createOrUpdateUser: function( next ){
                var userData;
                if ( locals.existingUser ){
                    userData = {
                        state: 'enabled',
                        website: locals.form.website,
                        isVerified: true,
                        services: locals.existingUser.services || {}
                    };

                    _.extend( userData.services[locals.authUser.type], {
                        isConfigured: true,
                        profileId: locals.authUser.profileId,
                        username: locals.authUser.username,
                        avatar: locals.authUser.avatar,
                        accessToken: locals.authUser.accessToken,
                        refreshToken: locals.authUser.refreshToken
                    });

                    locals.existingUser.set( userData );

                    locals.existingUser.save( function ( error ){
                        if ( error ){
                            console.log( '[auth.confirm] - Error saving existing user.', error );
                            next( {message: 'Sorry, there was an error processing your account, please try again.'} );
                        }
                        else
                            next();
                    });
                }
                else {
                    userData = {
                        name: {
                            first: locals.form['name.first'],
                            last: locals.form['name.last']
                        },
                        email: locals.form.email,
                        password: Math.random().toString( 36 ).slice( - 8 ),
                        state: 'enabled',
                        website: locals.form.website,
                        isVerified: true,
                        services: {}
                    };

                    userData.services[locals.authUser.type] = {
                        isConfigured: true,
                        profileId: locals.authUser.profileId,
                        username: locals.authUser.username,
                        avatar: locals.authUser.avatar,
                        accessToken: locals.authUser.accessToken,
                        refreshToken: locals.authUser.refreshToken
                    };

                    locals.existingUser = new User.model( userData );
                    locals.existingUser.save( function ( error ){
                        if ( error ){
                            console.log( '[auth.confirm] - Error saving new user.', error );
                            next( {message: 'Sorry, there was an error processing your account, please try again.'} );
                        }
                        else
                            next();
                    });
                }
            },

            signIn: function( next ){
                if ( req.user )
                    res.redirect( req.cookies.target || '/' );
                else
                    doSignIn();
                next();
            }
        }, function ( error ){
            if ( error ){
                console.log( '[auth.confirm] - Issue signing user in.', error );
                req.flash( 'error', error.message || 'Sorry, there was an issue signing you in, please try again.' );
                res.redirect( '/signin' );
            }
        });
    }
};
