var keystone = require( 'keystone' ),
    async = require( 'async' ),
    services = {
        github: require( '../../lib/auth/github' ),
        facebook: require( '../../lib/auth/facebook' ),
        google: require( '../../lib/auth/google' )
    };

module.exports = function ( req, res, next ){
    if ( !req.params.service )
        res.redirect( '/signin' );
    else if ( !services[req.params.service] )
        res.redirect( '/signin' );
    else {
        if ( req.query.target )
            res.cookie( 'target', req.query.target );
        services[req.params.service].authenticateUser( req, res, next );
    }
};
