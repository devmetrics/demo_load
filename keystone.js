// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require( 'dotenv' ).load();

var keystone = require( 'keystone' );

keystone.init({
    'name': 'devemetrics.io',
    'brand': 'devemetrics.io',
    'less': 'public',
    'static': 'public',
    'favicon': 'public/favicon.ico',
    'views': 'templates/views',
    'view engine': 'jade',
    'emails': 'templates/emails',
    'auto update': true,
    'session': true,
    'auth': true,
    'user model': 'User',
    'cookie secret': 'cds4q1NI)k(2LD+_3gQf#5**QamZ[4b-i&*_&)ZDe?lt"=hKBHdsuQGCPvQg1D!I'
});

global.mongoose_instance = keystone.mongoose;

keystone.import('models');

keystone.set( 'siteUrl',
    process.env.PROTOCOL + '://' +
    process.env.HOSTNAME +
    (process.env.EXTERNAL_PORT != 80 ? ':' + process.env.EXTERNAL_PORT : '')
);

keystone.set( 'locals', {
    _: require( 'underscore' ),
    env: keystone.get( 'env' ),
    utils: keystone.utils,
    editable: keystone.content.editable
});

keystone.set( 'routes', require('./routes') );

keystone.set( 'email locals', {
    logo_src: '/images/logo-email.gif',
    logo_width: 194,
    logo_height: 76,
    theme: {
        email_bg: '#f9f9f9',
        link_color: '#2697de',
        buttons: {
            color: '#fff',
            background_color: '#2697de',
            border_color: '#1a7cb7'
        }
    }
});

keystone.set( 'email rules', [{
    find: '/images/',
    replace: keystone.get( 'siteUrl' ) + '/images/'
}, {
    find: '/keystone/',
    replace: keystone.get( 'siteUrl' ) + '/keystone/'
}]);

keystone.set( 'email tests', require( './routes/emails' ) );

keystone.set( 'nav', {
    'posts': ['posts', 'post-categories'],
    'enquiries': 'enquiries',
    'users': 'users'
});

keystone.start();
